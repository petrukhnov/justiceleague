import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.RotateMoveController;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

public class HelloNxt {
	
	private static final NXTRegulatedMotor M_L = Motor.C;
	private static final NXTRegulatedMotor M_R = Motor.B;
	private static final NXTRegulatedMotor M_M = Motor.A;
	
	private static final int CLAW_CLOSED = M_M.getPosition();
	private static final int CLAW_OPEN = M_M.getPosition()+45;
	//private static int clawPos = CLAW_CLOSED;  //closed as initial position
	
	private static final LightSensor S_LIGHT = new LightSensor(SensorPort.S4);
	private static final UltrasonicSensor S_SONAR = new UltrasonicSensor(SensorPort.S1);
	
	public static void main(String[] args) {
		ready();
	}
	
	/** 
	 * add code here
	 */
	public static void start() {
		//followLine();
		//displayLightIntensity();
		//displayDistance();
		level2();
		LCD.drawString("Ready for level 4 ", 0, 1);
		Button.waitForAnyPress();
		level4();
		LCD.drawString("Ready for level 5 ", 0, 1);
		Button.waitForAnyPress();
		level5();
		//
		
		//motorTest();
		
		//clawOpen();
		//Button.waitForAnyPress();
		//clawClose();
		//Button.waitForAnyPress();
		
		//motorTestForward();
		//level4();
	}
	
	public static void ready() {
		LCD.drawString("Ready ", 0, 1);
		Button.waitForAnyPress();
		start();
	}
	
	/**
	 * Check in every loop or long operation
	 */
	public static void checkReset() {
		
		if (Button.ESCAPE.isDown()) {
			System.exit(0);
		}
		
		if (Button.LEFT.isDown()) {
			reset();
			ready();
		}
	}
	
	public static void reset() {
		M_M.stop();
		M_L.stop();
		M_R.stop();
	}
	
	public static void motorTestForward() {
	
		
		M_R.rotate(1000,true);
		M_L.rotate(1000,false);
	}
	
	public static void motorTest() {
		
		LCD.drawString("Test  0  ", 0, 1);
		checkReset();
		
		M_R.rotate(400);
		LCD.drawString("Test  1  ", 0, 1);
		checkReset();
		
		M_R.rotate(400);
		LCD.drawString("Test  2  ", 0, 1);
		checkReset();
		
		M_R.rotate(400);
		LCD.drawString("Test  3  ", 0, 1);
		checkReset();
		
		M_R.rotate(400);
		LCD.drawString("Test  4  ", 0, 1);
		checkReset();
		
	}
	
	public static void findLineStart() {
		
	}
	
	
	public static void clawClose() {
		M_M.rotate(CLAW_CLOSED-M_M.getPosition());
		
	}
	
	public static void clawOpen() {
		M_M.rotate(CLAW_OPEN-M_M.getPosition());
	}
	
	public static void forward(int angles) {
		M_R.rotate(angles,true);
		M_L.rotate(angles,false);
	}
	
	/**
	 * - is to the left
	 * + to the right
	 * @param angle
	 */
	public static void turn(int angle) {
		M_L.rotate(angle*2, true);
    	M_R.rotate(-angle*2, false);
	}
	
	//monolith
	public static void level1() {
		//go to button
		turn(-18);
		forward(2000);
    	turn(30);// hit
    	
    	//hit button 2 more times  (just in case)
    	turn(-30);
    	turn(30);
    	turn(-30);
    	turn(30);
    	
		//go to box
    	//turn(-180);
    	M_R.rotate(790);
    	clawOpen();
    	forward(640);
    	clawClose();
		
		//grab box
    	
    	M_R.rotate(-300);
    	turn(80);
    	//turn(105);
		
		//go to drop area
    	while(S_SONAR.getDistance()>50) {
    		 forward(100);
    	}
    	
    	turn(-90);
		
		//drop box
		
		//go to exit
		
		//(or get another box, and continue to level 2)

		
	}
	
	//holes
    public static void level2() {
    	
    	
    	
    	//to keep claw more tight
    	M_M.rotate(-5);
    	
    	//go to wall
    	
    	forward(500);
    	turn(45);
    	forward(500);
    	turn(45);
    	forward(1600);
    	turn(-90);
    	
    	while (S_SONAR.getDistance() > 10) {
    		forward(20);
    	}
    	turn(105);
    	
    	
    	/*while (S_SONAR.getDistance() > 11) {
    		forward(30);
    	}
    	
    	turn(95);
    	forward(2000);
    	
    	turn(-90);
    	while (S_SONAR.getDistance() > 11) {
    		forward(30);
    	}
    	*/
		//go to place 1
    	
    	
    	//check if open - exit
    	if(S_SONAR.getDistance() > 200) {
    		turn(-45);
    		forward(60);
    		turn(45);
    		
    		exitLevel2();
    	} else {
    		
    		//Button.waitForAnyPress();
    		//go to place 2
    		turn(90);
    		forward(500);
    		turn(-90);
    		
    		// check if open - exit
    		if(S_SONAR.getDistance() > 200) {
    			exitLevel2();
        	} else {
        		
        		//Button.waitForAnyPress();
        		
        		//go to place 3 
        		turn(90);
        		forward(600);
        		turn(-90);
        		
        		//exit
        		exitLevel2();
        	}
    	}
	}
    
    public static void exitLevel2() {
    	forward(3000);
    }
    
    //nixu rotating
    public static void level3() {
    	//just go through with adjustment rotation
    	
    	
    }

    //nails
    public static void level4() {
    	//just go through as coded
    	
    	//to keep claw more tight
    	M_M.rotate(-5);
    	
    	//todo more tests
    	
    	turn(-65);
    	forward(1100);
    	turn(45);
    	forward(985);
    	//x point
    	turn(62);
    	forward(2100);
    	turn(-47);
    	forward(600);
    	
    }

    //labyrinth
    public static void level5() {
    	//  find line
    	
    	//follow line
    	
    	followLine();
    	
    	//detect line ends
    	
    	//full ahead to finish
    }
    
    public static void displayLightIntensity() {
    	while(true) {
			LCD.drawString("Light: "+S_LIGHT.readValue()+"   ", 0, 1);
			checkReset();
		}
    }
    
    public static void displayDistance() {
    	while(true) {
			LCD.drawString("Sonar: "+S_SONAR.getDistance()+"   ", 0, 1);
			checkReset();
		}
    }
	
	
    public static void turnRight90() {
    	M_L.rotate(180, true);
    	M_R.rotate(-180, false);
    
    }
    
    public static void turnLeft90() {
    	M_L.rotate(-180, true);
    	M_R.rotate(180, false);
    }
    
    public static void followLine() {
    	
    	
    	//max black 46
    	//min black 20
    	//max white 60
    	//min white 55
    	final int blackColor = 47;
    	
		final RotateMoveController pilot = new DifferentialPilot(5, 13, M_L, M_R, false);
        pilot.setRotateSpeed(180);
        /**
         * this behavior wants to take control when the light sensor sees the line
         */
		Behavior DriveForward = new Behavior() {
			public boolean takeControl() {
				return S_LIGHT.readValue() <= blackColor;
			}
			
			public void suppress() {
				pilot.stop();
			}
			public void action() {
				LCD.drawString("Forward ", 0, 1);
				pilot.forward();
                while(S_LIGHT.readValue() <= blackColor) {
                	checkReset();
                	Thread.yield(); //action complete when not on line
                }
			}					
		};
		
		Behavior OffLine = new Behavior() {
			private boolean suppress = false;
			
			public boolean takeControl() {
				return S_LIGHT.readValue() > blackColor;
				}

			public void suppress() {
				suppress = true;
			}
			
			public void action() {
				LCD.drawString("Offline ", 0, 1);
				int sweep = 10;
				while (!suppress) {
					checkReset();
					pilot.rotate(sweep,true);
					while (!suppress && pilot.isMoving()) {
						checkReset();
						Thread.yield();
					}
					sweep *= -2;
				}
				pilot.stop();
				suppress = false;
			}
		};

		Behavior[] bArray = {OffLine, DriveForward};
        LCD.drawString("Line ", 0, 1);
        (new Arbitrator(bArray)).start();
    }
}
